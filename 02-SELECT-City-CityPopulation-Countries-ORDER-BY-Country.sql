SELECT 
    city.Name, city.Population, country.name AS Country
FROM
    city
        LEFT JOIN
    country ON city.CountryCode = country.Code
ORDER BY Country;
