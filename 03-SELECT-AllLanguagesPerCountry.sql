SELECT GROUP_CONCAT(language) AS 'All languages per country', country.Name AS Country
FROM countrylanguage 
LEFT JOIN country
ON countrylanguage.countrycode = country.code
GROUP BY country.name