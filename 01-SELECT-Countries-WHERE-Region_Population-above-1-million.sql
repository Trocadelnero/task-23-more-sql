SELECT
    *
FROM
    country
GROUP BY region
HAVING  AVG(Population > 1000000)
